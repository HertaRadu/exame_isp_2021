package subject2;

import java.util.Date;

public class ThreadApp extends Thread{
    public ThreadApp(String name) {
        super(name);
    }

    public static void main(String[] args) {
        ThreadApp BThread1 = new ThreadApp("BThread1");
        ThreadApp BThread2 = new ThreadApp("BThread2");
        ThreadApp BThread3 = new ThreadApp("BThread3");



        BThread1.start();
        BThread2.start();
        BThread3.start();
    }

    public void start() {
        for (int i = 1; i <= 7; i++) {
            Date today = new Date();
            Date currentTime = new java.sql.Time(today.getTime()); System.out.println("current time: " + currentTime);

            System.out.println(getName() + "  " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}